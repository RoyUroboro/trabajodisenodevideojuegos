﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class EnemySpawner : MonoBehaviour
{

    //Spawn
    public GameObject EnemySpawn;
    public int xPosSpawn;
    public int zPosSpawn;
    public int enemyCount;

    void Start()
    {
        
        StartCoroutine(EnemyDrop());
    }

    IEnumerator EnemyDrop()
    {
        while (enemyCount < 5)
        {
            xPosSpawn = Random.Range(-20, 20);
            zPosSpawn = Random.Range(20, -20);

            Instantiate(EnemySpawn, new Vector3(xPosSpawn, 5, zPosSpawn), Quaternion.identity);
            yield return new WaitForSeconds(0.1f);
            enemyCount += 1;
        }
    }
    

}
