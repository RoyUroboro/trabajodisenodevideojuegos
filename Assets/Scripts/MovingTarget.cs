﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MovingTarget : MonoBehaviour
{

    //Moving
    NavMeshAgent navMeshAgent;
    NavMeshAgent path;
    public float timeNewPath;
    bool inCoRoutine;
    Vector3 target;


    


    // Start is called before the first frame update
    void Start()
    {
        
        navMeshAgent = GetComponent<NavMeshAgent>();
    }

    Vector3 getNewrandomPos()
    {
        float x = Random.Range(-20, 20);
        float y = Random.Range(0, 20);
        float z= Random.Range(20, -20);

        Vector3 pos = new Vector3(x, y, z);
        return pos;
    }

    IEnumerator DoSomething()
    {
        inCoRoutine = true;
        yield return new WaitForSeconds(timeNewPath);
        GetNewPath();
       
        inCoRoutine = false;
    }

    void GetNewPath()
    {
        target = getNewrandomPos();
        navMeshAgent.SetDestination(target);
    }

    // Update is called once per frame
    void Update()
    {
        if (!inCoRoutine)
        {
            StartCoroutine(DoSomething());
        }
    }

   
    
}
