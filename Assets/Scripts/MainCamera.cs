﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainCamera : MonoBehaviour
{
    public Camera FpsCamera;
    public float HorizontalSpeed; 
    public float VerticalSpeed;

    public float h;
    public float v;
    // Start is called before the first frame update
    void Start()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    //private void OnCollisionEnter(Collision collision)
    //{
    //    if (gameObject.CompareTag("wall"))
    //    {
    //        Rigidbody.velocity = Vector3.zero;
    //    }
    //}

    // Update is called once per frame
    void Update()
    {
        Cursor.visible = false;

        h = HorizontalSpeed * Input.GetAxis("Mouse X");
        v = VerticalSpeed * Input.GetAxis("Mouse Y");

        transform.Rotate(0, h, 0);
        FpsCamera.transform.Rotate(-v, 0, 0);

        if (Input.GetKey(KeyCode.A))
        {
            transform.Translate(-0.1f, 0, 0);
        }
        else
        {
            if (Input.GetKey(KeyCode.D))
            {
                transform.Translate(0.1f, 0, 0);
            }
            else
            {
                if (Input.GetKey(KeyCode.S))
                {
                    transform.Translate(0, 0, -0.1f);
                }
                else
                {
                    if (Input.GetKey(KeyCode.W))
                    {
                        transform.Translate(0, 0, 0.1f);
                    }
                    else
                    {
                      if (Input.GetKey(KeyCode.R))
                    {
                        
                        SceneManager.LoadScene( SceneManager.GetActiveScene().name );
                    }  
                    }
                }
            }
        }
    }
}
